import type { Reporter } from 'jest-allure/dist/Reporter';
import Client from "../../dev/http-client";
import type { CatMinInfo, CatsList } from "../../dev//types";


const HttpClient = Client.getInstance();
declare const reporter: Reporter;


describe("Домашняя работа по тестированию API котиков", () => {
    function randomString() {
        const dataSetUppercase = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
        const dataSetLowercase = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

        const length = 10
        let randomString = dataSetUppercase.charAt(Math.floor(Math.random() * dataSetUppercase.length));
        for (let i = 0; i <= length - 1; i++) {
            randomString += dataSetLowercase.charAt(Math.floor(Math.random() * dataSetLowercase.length));
        }
        return randomString
    }

    let catId;
    const cats: CatMinInfo[] = [{ name: randomString(), description: '', gender: 'male' }];

    beforeAll(async () => {
        reporter.startStep('Добавление тестового котика перед автотестами');

        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                reporter.addAttachment(
                    'testAttachment',
                    JSON.stringify(add_cat_response.body, null, 2),
                    'application/json'
                );
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов!');
        }

        reporter.endStep();
    });

    afterAll(async () => {
        reporter.startStep('Удаление тестого кота после автотестов');
        await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
        reporter.endStep();
    });

    it('Поиск котика по существующему id', async () => {
        reporter.startStep('Запрос на котика по id');
        const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, { responseType: 'json' });
        reporter.addAttachment(
            'testAttachment',
            JSON.stringify(response.body, null, 2),
            'application/json'
        );
        reporter.endStep();

        reporter.startStep('Проверка кода ответа');
        expect(response.statusCode).toEqual(200);
        reporter.endStep();

        reporter.startStep('Проверка, найден ли нужный котик');
        expect(response.body).toMatchObject({
            cat: {
                id: catId,
            }
        });
        reporter.endStep();
    });

    it('Поиск котика по некорректному id', async () => {
        let fakeId = -1
        reporter.startStep('Проверка возврата ошибки 404 при отправки запроса с некорректным id');
        await expect(
            HttpClient.post(`core/cats/get-by-id?id=${fakeId}`, { responseType: 'json', })
        ).rejects.toThrowError('Response code 404 (Not Found)');
        reporter.endStep();
    });

    it('Добавлениие описания котику', async () => {
        let checkDescription = "Тут очень важое описание для проверки";

        reporter.startStep('Запрос для нового описания');
        const response = await HttpClient.post(`core/cats/save-description`, {
            responseType: 'json',
            json: {
                catId: catId,
                catDescription: checkDescription,
            },
        });

        reporter.addAttachment(
            'testAttachment',
            JSON.stringify(response.body),
            'application/json'
        );
        reporter.endStep();


        reporter.startStep('Проверка кода ответа');
        expect(response.statusCode).toEqual(200);
        reporter.endStep();

        reporter.startStep('Проверка, верно ли добавилось описание');
        expect(response.body).toMatchObject({
            id: catId,
            description: checkDescription,
        });
        reporter.endStep();
    });

    it('Список котов, сгруппированный по группам', async () => {
        reporter.description('Тест, проверяющий верность структуры ответа');
        reporter.startStep('Запрос списка, сгруппированного по группам');
        const response = await HttpClient.get(`core/cats/allByLetter?limit=1`, { responseType: 'json' });

        reporter.addAttachment(
            'testAttachment',
            JSON.stringify(response.body, null, 2),
            'application/json'
        );
        reporter.endStep();

        reporter.startStep('Проверка кода ответа');
        expect(response.statusCode).toEqual(200);
        reporter.endStep();

        reporter.startStep('Проверка верности структуры ответа');
        expect(response.body).toEqual({
            groups: expect.arrayContaining([
                expect.objectContaining(
                    {
                        title: expect.any(String),
                        cats: expect.arrayContaining([
                            expect.objectContaining(
                                {
                                    id: expect.any(Number),
                                    name: expect.any(String),
                                    description: expect.any(String),

                                    // Все значения тегов - null (хотя судя по сваггеру, должны быть String),
                                    // а как это проверить внутри toEqual, в документации jest я не нашёл.
                                    // Хочу задать вопрос как расширить функционал expect.any().
                                    tags: expect.any(Object),

                                    gender: expect.any(String),
                                    likes: expect.any(Number),
                                    dislikes: expect.any(Number),
                                    count_by_letter: expect.any(String)
                                }
                            ),
                        ]),
                        count_in_group: expect.any(Number),
                        count_by_letter: expect.any(Number)
                    }
                ),
            ]),
            count_output: expect.any(Number),
            count_all: expect.any(Number)
        });
        reporter.endStep();
    });
});

