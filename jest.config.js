module.exports = {
  testMatch: ['<rootDir>/**/__tests__/**/*.ts'],
  transform: {
    '^.+\\.(ts)$': ['ts-jest', { tsconfig: '<rootDir>/tsconfig.json' }],
  },
  verbose: true,
  // testRunner: 'jest-circus/runner',
  setupFilesAfterEnv: ['jest-allure/dist/setup'],
};
